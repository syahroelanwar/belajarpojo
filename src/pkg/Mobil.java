/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg;

/**
 *
 * @author Syahrul
 */
public class Mobil {
    private int id;
    private String namaMobil;
    private String merk;
    private int harga;
    
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }
    
    public String getNamaMobil(){
        return namaMobil;
    }
    public void setNamaMobil(String namaMobil){
        this.namaMobil = namaMobil;
    }
    
    public String getMerk(){
        return merk;
    }
    public void setMerk(String merk){
        this.merk = merk;
    }
    
    public int getHarga(){
        return harga;
    }
    public void setHarga(int harga){
        this.harga = harga;
    }
} 

