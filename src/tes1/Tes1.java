/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tes1;
import java.util.Scanner;

/**
 *
 * @author Syahrul
 */
class Buku {
    private int id;
    private String judul;
    private String penerbit;
    private int tahun_terbit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public int getTahun_terbit() {
        return tahun_terbit;
    }

    public void setTahun_terbit(int tahun_terbit) {
        this.tahun_terbit = tahun_terbit;
    }
    

}

public class Tes1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner masukan = new Scanner(System.in);
        
        Buku m = new Buku();
        
        System.out.print("ID : ");
        int id = Integer.parseInt(masukan.nextLine());
        
        System.out.print("Judul : ");
        String jdl = masukan.nextLine();
        
        System.out.print("Penerbit : ");
        String pb = masukan.nextLine();
        
        System.out.print("Tahun Terbit : ");
        int th = masukan.nextInt();
        System.out.println();
        
        m.setId(id);
        m.setJudul(jdl);
        m.setPenerbit(pb);
        m.setTahun_terbit(th);
        
        System.out.println("------- Output -------");
        System.out.println("ID : "+m.getId());
        System.out.println("Judul : "+m.getJudul());
        System.out.println("Penerbit : "+m.getPenerbit());
        System.out.println("Tahun Terbit : "+m.getTahun_terbit());
    }
    
}
