/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tes1;

import pkg.Mobil;
import java.util.Scanner;

/**
 *
 * @author Syahrul
 */
public class Tes3 {
   public static void main(String[] args) {
        
        Scanner masukan = new Scanner(System.in);
        
        Mobil m = new Mobil();
        
        System.out.print("ID : ");
        int id = Integer.parseInt(masukan.nextLine());
        
        System.out.print("Nama Mobil : ");
        String nm = masukan.nextLine();
        
        System.out.print("Merk : ");
        String merk = masukan.nextLine();
        
        System.out.print("Harga : ");
        int hrg = masukan.nextInt();
        System.out.println();
        
        m.setId(id);
        m.setNamaMobil(nm);
        m.setMerk(merk);
        m.setHarga(hrg);
        
        System.out.println("------- Output -------");
        System.out.println("ID : "+m.getId());
        System.out.println("Nama Mobil : "+m.getNamaMobil());
        System.out.println("Merk : "+m.getMerk());
        System.out.println("Harga : "+m.getHarga());
    }    
}
