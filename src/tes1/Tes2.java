/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tes1;

import java.util.Scanner;

class Employee{
    private final String name;
    private final String id;
    private final int salary;
    
    public Employee(String name, String id, int salary){
        this.name = name;
        this.id = id;
        this.salary = salary;
    }
    public String getName(){
        return name;
    }
    public String getId(){
        return id;
    }
    public int getSalary(){
        return salary;
    }
}

/**
 *
 * @author Syahrul
 */
public class Tes2 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Name : ");
        String nm = input.nextLine();
        
        System.out.print("ID : ");
        String id = input.nextLine();
        
        System.out.print("Salary : ");
        int slr = input.nextInt();
        System.out.println();
        
        Employee em = new Employee(nm,id,slr);
        
        System.out.println("------Output Coba------");
        System.out.println("Name : "+em.getName());
        System.out.println("ID : "+em.getId());
        System.out.println("Salary : "+em.getSalary());
        
    }
}
